
  80c READER
  ==========

  WHAT
  ----

  This repository collects material ...
  


  NAMING CONVENTIONS
  ------------------

  format/year_author_title.extension
  => odt/2008_snelting_awkwardgestures.odt

  if the document is a collection of multiple files please make a subfolder
  => html/2008_snelting_awkwardgestures/*


# --------------------------------------------------------------------------- #
  H O W  T O
# --------------------------------------------------------------------------- #

  run: `make lokal`
   or  `src/lokalize.sh`
  to download the collection to your local machine.
  This collection is made from a public list to be edited
  at http://pad.constantvzw.org/p/eightycolumn-reader.list
  
  As this list may be edited by anybody the maintainers of this repository
  do not give any warranty regarding copyright or fitness for a particular
  purpose. Wherever possible the ambition is to derive and collect editable
  versions of the incoming documents within this repository in various formats.


