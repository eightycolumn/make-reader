#!/bin/bash

  LISTURL="http://pad.constantvzw.org/p/eightycolumn-reader.list/export/txt"
    SHDIR=`cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd`

  cd $SHDIR
  curl $LISTURL -o remote.list -s -L

( IFS=$'\n'
  for REMOTE in `cat remote.list | # FULL LIST
                 grep "^http"`  # HTTP ONLY
    do
      # IF REMOTE FILE EXISTS                           #
      # ----------------------------------------------- #
        if [ `curl -s -o /dev/null -IL  \
              -w "%{http_code}" $REMOTE` == '200' ]
        then LOKAL=`basename "$REMOTE" | #
                    echo -e $(sed 's/%/\\\x/g')`
  
      # IF LOKAL FILE EXISTS                            #
      # ----------------------------------------------- #
        if [ -f "$LOKAL" ]; then
  
      # DOWNLOAD IF REMOTE IS NEWER                     #
      # ----------------------------------------------- #
        if [ `curl "$REMOTE" -z "$LOKAL" -o "$LOKAL" \
              -s -L -w %{http_code}` == "200" ]; then
              echo "Download $LOKAL"
              curl "$REMOTE" -o "$LOKAL"
        else  echo "$LOKAL is up-to-date"
        fi;else
  
      # DOWNLOAD IF NO LOKAL FILE                       #
      # ----------------------------------------------- #
              curl "$REMOTE" -o "$LOKAL"
        fi;fi
  done        )

  rm remote.list
  cd - > /dev/null 2>&1

 exit 0;

